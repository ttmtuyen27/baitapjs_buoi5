function tinhDiemKhuVuc(khuVucValue) {
  switch (khuVucValue) {
    case "A": {
      var diemKhuVuc = 2;
      break;
    }
    case "B": {
      var diemKhuVuc = 1;
      break;
    }
    case "C": {
      var diemKhuVuc = 0.5;
      break;
    }
    default:
      var diemKhuVuc = 0;
  }
  return diemKhuVuc;
}

function tinhDiemDoiTuong(doiTuongValue) {
  switch (doiTuongValue) {
    case "1": {
      var diemDoiTuong = 2.5;
      break;
    }
    case "2": {
      var diemDoiTuong = 1.5;
      break;
    }
    case "3": {
      var diemDoiTuong = 1;
      break;
    }
    default:
      var diemDoiTuong = 0;
  }
  return diemDoiTuong;
}

document.getElementById("kiemTraDauRot").addEventListener("click", function () {
  var diem1 = document.getElementById("diemMon1").value * 1;
  var diem2 = document.getElementById("diemMon2").value * 1;
  var diem3 = document.getElementById("diemMon3").value * 1;
  var khuVucValue = document.getElementById("khuVuc").value;
  var doiTuongValue = document.getElementById("doiTuong").value;
  var diemKhuVuc = tinhDiemKhuVuc(khuVucValue);
  var diemDoiTuong = tinhDiemDoiTuong(doiTuongValue);
  var diemChuan = document.getElementById("diemChuan").value * 1;
  var diemTong = diem1 + diem2 + diem3 + diemKhuVuc * 1 + diemDoiTuong * 1;
  var kiemTraDau = true;
  console.log(
    { diem1 },
    { diem2 },
    { diem3 },
    { khuVucValue },
    { diemKhuVuc },
    { doiTuongValue },
    { diemDoiTuong },
    { diemChuan },
    { diemTong }
  );
  if (diem1 == 0 || diem2 == 0 || diem3 == 0 || diemTong < diemChuan)
    kiemTraDau = false;
  if (kiemTraDau)
    document.getElementById(
      "ketQuaBai1"
    ).innerHTML = `Bạn đã đậu. Tổng điểm là ${diemTong}.`;
  else
    document.getElementById(
      "ketQuaBai1"
    ).innerHTML = `Bạn đã rớt. Tổng điểm là ${diemTong}.`;
});
