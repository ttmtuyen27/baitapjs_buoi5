function tinhTienDien(soDien) {
  var tienDien = 0;
  if (soDien <= 50) tienDien = soDien * 500;
  else if (soDien <= 100) tienDien = 500 * 50 + (soDien - 50) * 650;
  else if (soDien <= 200) tienDien = 500 * 50 + 650 * 50 + (soDien - 100) * 850;
  else if (soDien <= 350)
    tienDien = 500 * 50 + 650 * 50 + 850 * 100 + (soDien - 200) * 1100;
  else
    tienDien =
      500 * 50 + 650 * 50 + 850 * 100 + 150 * 1100 + (soDien - 350) * 1300;
  return tienDien;
}

document.getElementById("tinhTienDien").addEventListener("click", function () {
  var userName = document.getElementById("userName").value;
  var soDien = document.getElementById("soDien").value * 1;
  var tongTien = tinhTienDien(soDien);
  document.getElementById("ketQuaBai2").innerHTML = `<p>Tên: ${userName}</p>
    <p>Số tiền điện phải trả: ${tongTien}</p>`;
});
